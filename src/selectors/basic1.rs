use crate::selectors::{default_output, get_vec};

pub fn main(data: &scraper::Html, tty: &bool, w: usize) {
    let elements = get_vec(data, "div.HwtpBd.gsrt.PZPZlf.kTOYnf");
    
    if let Some(content) = elements.get(0) {
        if *tty {
            default_output(content, w);
        } else {
            println!("{}", content);
        }
    } else {
        eprintln!("No elements found with the specified selector.");
    }
}
