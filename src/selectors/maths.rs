use crate::selectors::{default_output, get_vec};
use scraper::Html;

pub fn main(data: &Html, tty: &bool, w: usize) {
    // Extract the content from the specified selector
    let extracted_data = get_vec(data, "span.qv3Wpe");

    // If the extracted data is not empty, process it
    if let Some(first_item) = extracted_data.first() {
        let mut processed_string = first_item.to_string();
        
        // Removing specific characters based on assumed requirements
        if processed_string.len() > 2 {
            processed_string.remove(processed_string.len() - 1); // Remove the last character
            processed_string.remove(processed_string.len() - 1); // Remove the second last character
            processed_string.remove(0); // Remove the first character
        }

        // Output the result based on the TTY flag
        match tty {
            true => default_output(&processed_string, w),
            false => println!("{}", processed_string),
        }
    } else {
        eprintln!("No data found for the given selector.");
    }
}
