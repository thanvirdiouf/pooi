use comfy_table::{
    modifiers::{UTF8_ROUND_CORNERS, UTF8_SOLID_INNER_BORDERS},
    presets::UTF8_FULL,
    Attribute, Cell, CellAlignment, ContentArrangement, Row, Table,
};
use scraper::{Html, Selector};

pub fn main(data: &Html, tty: &bool, w: usize) {
    let mut titles: Vec<String> = vec![];

    for element in data.select(&Selector::parse("a.ct5Ked.klitem-tr.PZPZlf").unwrap()) {
        if let Some(title) = element.value().attr("title") {
            titles.push(title.to_string());
        }
    }

    if *tty {
        let mut table = Table::new();
        table.load_preset(UTF8_FULL);
        table.apply_modifier(UTF8_ROUND_CORNERS);
        table.apply_modifier(UTF8_SOLID_INNER_BORDERS);
        table.set_content_arrangement(ContentArrangement::Dynamic);

        if w >= 100 {
            table.set_width(100);
        }

        for title in &titles {
            table.add_row(Row::from(vec![
                Cell::new(title)
                    .set_alignment(CellAlignment::Center)
                    .add_attribute(Attribute::Bold),
            ]));
        }

        println!("{}", table);
    } else {
        println!("{}", titles.join("\n"));
    }
}
