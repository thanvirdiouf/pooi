use crate::selectors::{default_output, get_vec};
use scraper::Html;

pub fn main(data: &Html, tty: &bool, w: usize) {
    let x = get_vec(data, "div.kno-rdesc");
    match tty {
        true => default_output(&x[1], w), // Pass a reference to the string
        false => println!("{}", x[1])
    }
}
